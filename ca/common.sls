{% from slspath + '/map.jinja' import ca with context %}

create_pki_directory:
  file.directory:
    - name: {{ ca.pki_directory }}
    - mode: 755
    - user: root
    - group: root

create_pki_issued_certs_directory:
  file.directory:
    - name: {{ ca.pki_directory }}/issued_certs
    - require:
      - file: create_pki_directory

create_pki_private_directory:
  file.directory:
    - name: {{ ca.pki_directory }}/private
    - user: root
    - group: ssl-cert
    - mode: 710
    - require:
      - file: create_pki_directory

{%- if ca.m2crypto == "pip" or ca.m2crypto == "pip3" %}
install_pip:
  pkg.installed:
  {%- if ca.m2crypto == "pip" %}
    - name: python-pip
  {%- endif %}
  {%- if ca.m2crypto == "pip3" %}
    - name: python3-pip
  {%- endif %}
install_m2crypto_pip_deps:
  pkg.installed:
    - pkgs:
      - build-essential
      - python3-dev
      - python-dev
      - libssl-dev
      - swig
    - require:
      - install_pip
{%- endif %}

install_m2crypto:
  {%- if ca.m2crypto == "pip" or ca.m2crypto == "pip3" %}
  pip.installed:
    - name: m2crypto
  {%- if ca.m2crypto == "pip" %}
    - bin_env: '/usr/bin/pip'
  {%- endif %}
  {%- if ca.m2crypto == "pip3" %}
    - bin_env: '/usr/bin/pip3'
  {%- endif %}
    - require:
      - install_m2crypto_pip_deps
  {%- endif %}
  {%- if ca.m2crypto == "pkg" %}
  pkg.installed:
    - name: python-m2crypto
  {%- endif %}
