{% set had_include = False %}

{%- if "server" in salt['pillar.get']('ca:services') %}
{%- if not had_include %}
include:
{%- endif %}
{% set had_include = True %}
  - .server
  - .cert
{%- endif %}
{%- if "client" in salt['pillar.get']('ca:services') %}
{%- if not had_include %}
include:
{% set had_include = True %}
{%- endif %}
  - .client
  - .cert
{%- endif %}
{%- if "certonly" in salt['pillar.get']('ca:services:certonly') %}
{%- if not had_include %}
include:
{% set had_include = True %}
{%- endif %}
  - .cert
{%- endif %}
