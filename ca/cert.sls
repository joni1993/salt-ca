{% from slspath + '/map.jinja' import ca with context %}

create_local_ca_certificates_directory:
  file.directory:
    - name: /usr/local/share/ca-certificates

import_ca_certificate_from_mine:
  x509.pem_managed:
    - name: {{ ca.ca_certificate_file }}
    - text: {{ salt['mine.get'](ca.server_id, ca.mine_ca_func_id)[ca.server_id][ca.mine_ca_crt_path]|replace('\n', '') }}
    - require:
      - file: create_local_ca_certificates_directory

{%- if ca.get_crl %}
import_ca_crl_from_mine:
  x509.pem_managed:
    - name: {{ ca.ca_crl_file }}
    - text: {{ salt['mine.get'](ca.server_id, ca.mine_ca_crl_func_id)[ca.server_id][ca.mine_ca_crl_path]|replace('\n', '') }}
    - require:
      - file: create_local_ca_certificates_directory
{%- endif %}

import_ca_certificate_to_store:
  cmd.wait:
    - name: /usr/sbin/update-ca-certificates
    - watch:
      - x509: import_ca_certificate_from_mine
