{% from slspath + '/map.jinja' import ca with context %}

include:
  - .common

salt_minion_for_ca_running:
  cmd.run:
    - name: 'salt-call service.restart salt-minion'
    - bg: True
    - onchanges:
      - file: /etc/salt/minion.d/signing_policies_version

install_signing_policies_version:
  file.managed:
    - name: /etc/salt/minion.d/signing_policies_version
    - contents:
      - {{ ca.server.signing_policies_version }}

{% if 'signing_policies_file_template' in ca.server %}
{% set signing_policies_file_template = ca.server.signing_policies_file_template %}
{% else %}
{% set signing_policies_file_template = 'salt://ca/templates/signing-policies.conf' %}
{% endif %}
install_signing_policies:
  file.managed:
    - name: /etc/salt/minion.d/signing_policies.conf
    - source: {{ signing_policies_file_template }}
    - template: jinja
    - context:
      slspath: {{ slspath }}

create_ca_key:
  x509.private_key_managed:
    - name: {{ ca.pki_directory }}/private/{{ ca.server.certificate.file_key }}
    - bits:  {{ ca.server.certificate.key_bits }}
    - backup: True
    - user: root
    - group: ssl-cert
    - mode: 640
    - require:
      - file: create_pki_directory

create_ca_certificate:
  x509.certificate_managed:
    - name: {{ ca.pki_directory }}/{{ ca.server.certificate.file }}
    - signing_private_key: {{ ca.pki_directory }}/private/{{ ca.server.certificate.file_key }}
{%- for parameter, value in ca.server.certificate.parameters.items() %}
    {%- if value != None and value != "" %}
    {%- if value is number or value is string %}
    - {{ parameter }}: {{ value }}
    {%- endif %}
    {%- endif %}
{%- endfor %}
    - user: root
    - group: ssl-cert
    - mode: 644
    - days_remaining: 0
    - backup: True
    - require:
      - file: create_pki_directory
      - x509: create_ca_key

create_ca_crl:
  x509.crl_managed:
    - name: {{ ca.pki_directory }}/{{ ca.server.crl_file }}
    - signing_private_key: {{ ca.pki_directory }}/private/{{ ca.server.certificate.file_key }}
    - signing_cert: {{ ca.pki_directory }}/{{ ca.server.certificate.file }}
    - user: root
    - group: ssl-cert
    - mode: 644
    - digest: sha256
    - days_remaining: 30
    - backup: True
    - require:
      - file: create_pki_directory
      - x509: create_ca_key
      - x509: create_ca_certificate

send_ca_to_mine:
  module.run:
    - name: mine.send
    - func: {{ ca.mine_ca_func_id }}
    - kwargs:
        glob_path: {{ ca.pki_directory }}/{{ ca.server.certificate.file }}
        mine_function: x509.get_pem_entries
    - onchanges:
      - x509: {{ ca.pki_directory }}/{{ ca.server.certificate.file }}
    - require:
       - install_m2crypto

send_ca_crl_to_mine:
 module.run:
   - name: mine.send
   - func: {{ ca.mine_ca_crl_func_id }}
   - kwargs:
       glob_path: {{ ca.pki_directory }}/{{ ca.server.crl_file }}
       mine_function: x509.get_pem_entries
   - onchanges:
     - x509: {{ ca.pki_directory }}/{{ ca.server.crl_file }}
   - require:
      - install_m2crypto
