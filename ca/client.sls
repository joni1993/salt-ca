{% from slspath + '/map.jinja' import ca with context %}

include:
  - .common

create_key_for_cert:
  x509.private_key_managed:
    - name: {{ ca.pki_directory }}/private/{{ salt['grains.get']('fqdn') }}.key
    - bits:  {{ ca.client.certificate.key_bits }}
    - backup: True
    - user: root
    - group: ssl-cert
    - mode: 640
    - require:
      - sls: {{ slspath|replace("/", ".") }}.common

request_cert_from_ca:
  x509.certificate_managed:
    - name: {{ ca.pki_directory }}/{{ salt['grains.get']('fqdn') }}.crt
    - ca_server: {{ ca.server_id }}
    - signing_policy: {{ ca.client.signing_policy }}
    - public_key: {{ ca.pki_directory }}/private/{{ salt['grains.get']('fqdn') }}.key
    - CN: {{ salt['grains.get']('fqdn') }}
    - subjectAltName: "DNS:{{ salt['grains.get']('fqdn') }}"
    - days_remaining: 30
    - backup: True
    - require:
      - sls: {{ slspath|replace("/", ".") }}.common
      - x509: create_key_for_cert
