=======
salt-ca
=======

Install and configure the a CA with Salt remote signing

.. note::

    This implements the a CA based on the Salt documentation from the x509 state.
    <https://docs.saltstack.com/en/latest/ref/states/all/salt.states.x509.html>

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

Available states
================

.. contents::
    :local:

``ca``
-----------

Installs all states based on pillar (ca.server or ca.client or ca.certonly)


``ca.server``
-----------------

Sets up a Salt CA server instance.

``ca.client``
-----------------

Sets up a Salt CA client instance, generates a key and gets it signed by the ca.

``ca.cert``
-----------------

Only requests and imports the CA certificate from the CA.
